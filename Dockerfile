FROM rust:latest
RUN apt update && apt install -y \
    clang \
    mold \
    libwayland-dev \
    librust-alsa-sys-dev \
    librust-libudev-sys-dev \
    librust-xkbcommon-dev \
    && rm -rf /var/lib/apt/lists/*
CMD ["cargo", "build", "--release"]
